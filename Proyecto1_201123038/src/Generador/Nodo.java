/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Generador;

/**
 *
 * @author Yess
 */
import java.util.ArrayList;

public class Nodo {
    
    public String val;
    public String tipo;
    public int num;
    public int linea;
    public int columna;
    public ArrayList<Nodo> hijos;
    
    public Nodo(){}
    
    public Nodo(String valor){
        this.val = valor;
        this.hijos = new ArrayList<Nodo>();
    }
    
    public Nodo(String valor,int lin,int col){
        this.val = valor;
        this.linea = lin;
        this.columna = col;
        this.hijos = new ArrayList<Nodo>();
    }
}
