/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Generador;
import Analizador.*;
import java.awt.Color;
import java.io.BufferedReader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;


/**
 *
 * @author Yess
 */
public class Arbolito {

ArrayList<Clase> clases;
String path;
String nombre;


Arbolito(){
    path="";
    nombre="";
    clases= new ArrayList<>();
}

Clase temporal= new Clase();
ArrayList<Metodos_Funciones>metodostemporales = new ArrayList<>();
Metodos_Funciones mftemporal= new Metodos_Funciones();
boolean overridetemporal=false;

public void recorrerClases(Nodo nodo){
    
    if(nodo.val.equals("clase")){
      temporal= new Clase();  
      if(nodo.hijos.size()==3){
          temporal.nombre=nodo.hijos.get(0).val;
          temporal.extiende=nodo.hijos.get(2).val;
      }else{
          temporal.nombre=nodo.hijos.get(0).val;
      }
      recorrerClases(nodo.hijos.get(1));
    }else if(nodo.val.equals("clases")){
        for(int i=0;i<nodo.hijos.size();i++){
            recorrerClases(nodo.hijos.get(i));
      if(!"".equals(temporal.nombre)){
          //verificar que son metodos y que funciones
        clases.add(temporal);
        temporal=new Clase();
        
      }

        }
    }else if(nodo.val.equals("inicio")){
        for(int i=0;i<nodo.hijos.size();i++){
            recorrerClases(nodo.hijos.get(i));
        }
    }else if(nodo.val.equals("codigo")){
        
        for(int i=0;i<nodo.hijos.size();i++){
            recorrerCodigo(nodo.hijos.get(i));
            if(!"".equals(mftemporal.nombre)){
                System.out.println("metodo temporal: "+mftemporal.nombre +" \t"+ mftemporal.tipo+" \t"+mftemporal.acceso+" \t"+mftemporal.nivel);
                
                mftemporal.override=overridetemporal;
                metodostemporales.add(mftemporal);
                mftemporal=new Metodos_Funciones();
                overridetemporal=false;
            }
        //declaracion  y asignacion tambien verificar 
        }
    
    }
    
}

public void recorrerCodigo(Nodo nodo){
        if(nodo.val.equals("metodo1")){
            mftemporal.tipo="metodo";
            mftemporal.nivel=temporal.nombre;
            if(nodo.hijos.size()==2){
                mftemporal.acceso=nodo.hijos.get(0).val;
                recorrerCodigo(nodo.hijos.get(1));
            }else{
                recorrerCodigo(nodo.hijos.get(0));
            }
        }else if(nodo.val.equals("Metodo")){
            mftemporal.nombre=nodo.hijos.get(0).val;
            for(int i=0;i<nodo.hijos.size();i++){
                recorrerCodigo(nodo.hijos.get(i));
            }
        }else if(nodo.val.equals("funcion1")){
            mftemporal.tipo="funcion";
            mftemporal.nivel=temporal.nombre;
            if(nodo.hijos.size()==2){
                mftemporal.acceso=nodo.hijos.get(0).val;
                recorrerCodigo(nodo.hijos.get(1));
            }else{
                recorrerCodigo(nodo.hijos.get(0));
            }
        }else if(nodo.val.equals("Funcion")){
            mftemporal.nombre=nodo.hijos.get(0).val;
            mftemporal.tipo_ret=nodo.hijos.get(1).val;
            for(int i=0;i<nodo.hijos.size();i++){
                recorrerCodigo(nodo.hijos.get(i));
            }
        }else if(nodo.val.equals("parametro")){
            Variable variable = new Variable();
            variable.acceso=mftemporal.nombre;
            variable.nombre= nodo.hijos.get(1).val;
            variable.tipo=nodo.hijos.get(0).val;
            mftemporal.parametros.add(variable);
            if(nodo.hijos.size()==3){
                recorrerCodigo(nodo.hijos.get(2));
            }
        }else if(nodo.val.equals("Constructor")){
            mftemporal.tipo="constructor";
            mftemporal.nombre=nodo.hijos.get(0).val;
            mftemporal.nivel=temporal.nombre;
            recorrerCodigo(nodo.hijos.get(1));
        }else if(nodo.val.equals("override")){
            overridetemporal=true; 
        }else if(nodo.val.equals("declaracion1")){
            for(int i=0;i<nodo.hijos.size();i++){
                recorrerCodigo(nodo.hijos.get(i));
            } 
        }else if(nodo.val.equals("asignacion")){
            //buscar si existe la variable
            
            for(int i=0;i<temporal.variables.size();i++){
                if(temporal.variables.get(i).nombre.equals(nodo.hijos.get(0).val)){
                    //verificar si ls tipos coinciden
                    if(temporal.variables.get(i).tipo.equals(nodo.hijos.get(1).hijos.get(0).val)){
                        temporal.variables.get(i).valor=nodo.hijos.get(1).hijos.get(1).val;
                    }else{
                    //error semantico no coinciden tipos
                    }
                }
            }//sino error semnatico no esta declarada la variable
            
        }else if(nodo.val.equals("declaracion")){
            //falta verificar si ya existe error semantico ya existe
            
            
            for(int i=0;i<nodo.hijos.get(0).hijos.size();i++){
                Variable variable = new Variable();
                variable.tipo=nodo.hijos.get(1).val;
                variable.nombre=nodo.hijos.get(0).hijos.get(i).val;
                variable.acceso=temporal.nombre;
                temporal.variables.add(variable);
                
            }
        }else if(nodo.val.equals("asigdec")){
            for(int i=0;i<nodo.hijos.get(0).hijos.size();i++){
                Variable variable = new Variable();
                variable.nombre=nodo.hijos.get(0).hijos.get(i).val;
                variable.tipo=nodo.hijos.get(1).val;
                variable.acceso=temporal.nombre;
                    //verificar si ls tipos coinciden
                
                if(nodo.hijos.get(2).hijos.get(0).val.equals(variable.tipo)){
                    variable.valor=nodo.hijos.get(2).hijos.get(1).val;
                    temporal.variables.add(variable);
                }else{
                  //error semantico no coinciden tipos

                }
            }
         }else if(nodo.val.equals("sentencia")){
                 mftemporal.sentencias=nodo;
         }
    
    }

public void inicioArbolito(Nodo raiz){
    recorrerClases(raiz);
    ordenarMetodos();
    extenderMetodos();
    pruebas();
    instrucciones();           
    System.out.println("done");
           
}

String cade="";
int metf=0;
public void instrucciones(){

    for(int i=0;i<clases.size();i++){
        for(int j=0;j<clases.get(i).metodos.size();j++){
            for(int k=0;k<clases.get(i).metodos.get(j).sentencias.hijos.size();k++){
                metf=1;
                parame1(clases.get(i).metodos.get(j).sentencias.hijos.get(k),i,j);
                cade=""; metf=0;
            }
        }
       
        for(int j=0;j<clases.get(i).funciones.size();j++){
             for(int k=0;k<clases.get(i).funciones.get(j).sentencias.hijos.size();k++){
                 metf=2;
                recorrerSentencias(clases.get(i).funciones.get(j).sentencias.hijos.get(k),i,j);
                cade="";metf=0;
            }
        }
    }
}
Nodo tmp= new Nodo();
public void recorrerSentencias(Nodo nodo,int clase, int met){
String cond="";
    switch(nodo.val){

        case "if1":
            cond=parame(nodo.hijos.get(0).hijos.get(0),clase,met);
            obtenerCondicion(cond);
            //sin else
            break;
        
        case "if2":
            cond=parame(nodo.hijos.get(0).hijos.get(0),clase,met);
            obtenerCondicion(cond);
            //con else
            //si es falso validar else hijo 1
            
            break;
            
        case "if3":
            cond=parame(nodo.hijos.get(0).hijos.get(0),clase,met);
            obtenerCondicion(cond);
            //con else ifs SIN else
            //validar else ifs hijo 1
            
            
            break;
            
        case "if4":
            cond=parame(nodo.hijos.get(0).hijos.get(0),clase,met);
            obtenerCondicion(cond);
            //con else ifs - else
            //validar else ifs hijo 1
            //si es falso validar else hijo 2
            
            break;
            
        case "switch":
            cond=parame(nodo.hijos.get(0),clase,met);
            obtenerCondicion(cond);
            
            
            for(int t=0;t<nodo.hijos.get(1).hijos.size();t++){
                cond=""; cade="";
                recorrerSentencias(nodo.hijos.get(1).hijos.get(t),clase,met);
            }
            break;
            
        case "while":
            cond=parame(nodo.hijos.get(0),clase,met);
            String c=obtenerCondicion(cond);
            System.out.println("cond1: "+cond);
            while(c.contains("true")){
                for(int i=0; i<nodo.hijos.get(1).hijos.size();i++){
                    recorrerSentencias(nodo.hijos.get(1).hijos.get(i),clase,met);
                    cond=parame(nodo.hijos.get(0),clase,met);
                    c=obtenerCondicion(cond);
                    System.out.println("cond 2: "+cond);
                    cade="";
                }
            }
            
            break;
        
        case "case":
            cond=parame(nodo.hijos.get(0),clase,met);
            obtenerCondicion(cond);
            break;
 
        case "def":

            
            break;
        
        case"dowhile":
            cond=parame(nodo.hijos.get(1),clase,met);
            obtenerCondicion(cond);
            
            break;
            
        case "repeat":
            cond=parame(nodo.hijos.get(1),clase,met);
            obtenerCondicion(cond);
            
            break;
            
        case "loop":
            
            break;
            
        case "for":
            cond=parame(nodo.hijos.get(0),clase,met);
            obtenerCondicion(cond);
            cade="";
            cond=parame(nodo.hijos.get(1),clase,met);
            obtenerCondicion(cond);
            
            break;
            
        case "continue":
            break;
            
        case "break":
            break;    
        
        case "asignacion":
            System.out.println("entro aca");
                if(metf==1){
                    for(int z=0;z<clases.get(clase).metodos.get(met).parametros.size();z++){
                        if(clases.get(clase).metodos.get(met).parametros.get(z).nombre.contains(nodo.hijos.get(0).val)){
                            cond=""; cade="";
                            cond=parame(nodo.hijos.get(1),clase,met);
                            clases.get(clase).metodos.get(met).parametros.get(z).valor=obtenerCondicion(cond);
                            cade="";
                        }else{
                            JOptionPane.showMessageDialog(null, "id: "+ nodo.hijos.get(0).val+" no ya declarada");
                        }
                    }
                    
                }else if(metf==2){
                    for(int z=0;z<clases.get(clase).funciones.get(met).parametros.size();z++){
                        if(clases.get(clase).funciones.get(met).parametros.get(z).nombre.contains(nodo.hijos.get(0).val)){
                            cond=""; cade="";
                            cond=parame(nodo.hijos.get(1),clase,met);
                            clases.get(clase).funciones.get(met).parametros.get(z).valor=obtenerCondicion(cond);
                            cade="";
                        }else{
                            JOptionPane.showMessageDialog(null, "id: "+ nodo.hijos.get(0).val+" no ya declarada");
                        }
                    }
                }
            cade="";
            break;
        
        case "asigdec":
            for(int y=0;y<nodo.hijos.get(0).hijos.size();y++){
                if(metf==1){
                    for(int z=0;z<clases.get(clase).metodos.get(met).parametros.size();z++){
                        if(clases.get(clase).metodos.get(met).parametros.get(z).nombre.contains(nodo.hijos.get(0).hijos.get(y).val)){
                            JOptionPane.showMessageDialog(null, "id: "+ nodo.hijos.get(0).hijos.get(y).val+" ya declarada");
                        }else{
                            Variable nueva = new Variable();
                            nueva.nombre=nodo.hijos.get(0).hijos.get(y).val;
                            nueva.acceso=clases.get(clase).metodos.get(met).nombre;
                            nueva.tipo=nodo.hijos.get(1).val;
                            cond=""; cade="";
                            cond=parame(nodo.hijos.get(2),clase,met);
                            nueva.valor=obtenerCondicion(cond);
                            clases.get(clase).metodos.get(met).parametros.add(nueva);
                        }
                    }
                    
                }else if(metf==2){
                    for(int z=0;z<clases.get(clase).funciones.get(met).parametros.size();z++){
                        if(clases.get(clase).funciones.get(met).parametros.get(z).nombre.contains(nodo.hijos.get(0).hijos.get(y).val)){
                            JOptionPane.showMessageDialog(null, "id: "+ nodo.hijos.get(0).hijos.get(y).val+" ya declarada");
                        }else{
                            Variable nueva = new Variable();
                            nueva.nombre=nodo.hijos.get(0).hijos.get(y).val;
                            nueva.acceso=clases.get(clase).funciones.get(met).nombre;
                            nueva.tipo=nodo.hijos.get(1).val;
                            cond=""; cade="";
                            cond=parame(nodo.hijos.get(2),clase,met);
                            nueva.valor=obtenerCondicion(cond);
                            clases.get(clase).funciones.get(met).parametros.add(nueva);
                        }
                    }
                }
            }
            cond="";
            cade="";
            break;
            
        case "declaracion":
            for(int y=0;y<nodo.hijos.get(0).hijos.size();y++){
                if(metf==1){
                    for(int z=0;z<clases.get(clase).metodos.get(met).parametros.size();z++){
                        if(clases.get(clase).metodos.get(met).parametros.get(z).nombre.contains(nodo.hijos.get(0).hijos.get(y).val)){
                            JOptionPane.showMessageDialog(null, "id: "+ nodo.hijos.get(0).hijos.get(y).val+" ya declarada");
                        }else{
                            Variable nueva = new Variable();
                            nueva.nombre=nodo.hijos.get(0).hijos.get(y).val;
                            nueva.acceso=clases.get(clase).metodos.get(met).nombre;
                            nueva.tipo=nodo.hijos.get(1).val;
                           
                            clases.get(clase).metodos.get(met).parametros.add(nueva);
                        }
                    }
                    
                }else if(metf==2){
                    for(int z=0;z<clases.get(clase).funciones.get(met).parametros.size();z++){
                        if(clases.get(clase).funciones.get(met).parametros.get(z).nombre.contains(nodo.hijos.get(0).hijos.get(y).val)){
                            JOptionPane.showMessageDialog(null, "id: "+ nodo.hijos.get(0).hijos.get(y).val+" ya declarada");
                        }else{
                            Variable nueva = new Variable();
                            nueva.nombre=nodo.hijos.get(0).hijos.get(y).val;
                            nueva.acceso=clases.get(clase).funciones.get(met).nombre;
                            nueva.tipo=nodo.hijos.get(1).val;
                           
                            clases.get(clase).funciones.get(met).parametros.add(nueva);
                        }
                    }
                }
            }
            break;
        default:
            break;
    
    }

}
public String obtenerCondicion(String entrada){
        String resul="";
//        String entrada;
  //      entrada = txtTokens.getText();
        if(!"".equals(entrada)){
            try {
            scanner lexico3 = new scanner(new BufferedReader( new StringReader(entrada)));
            Sintactico sintactico3=new Sintactico(lexico3);
             sintactico3.parse();
                //      
                System.out.println("resultado:  "+sintactico3.resulta);
              resul=""+sintactico3.resulta;
            }
            // TODO add your handling code here:
            catch (Exception ex) {
                Logger.getLogger(Principal.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
            if(resul.contains("true")||resul.contains("false")){
            }else{
            JOptionPane.showMessageDialog(null, "Error Semantico: Condicion no devuelve valor" );
            }
        return resul;
}  
public String parame(Nodo nodo,int clase, int met){
        if(nodo.val=="("||nodo.val==")"||nodo.val=="{"||nodo.val=="}"||nodo.val=="["||nodo.val=="]"
                ||nodo.val=="=="||nodo.val=="!="||nodo.val=="<"||nodo.val==">"||nodo.val=="<="||nodo.val==">="
                ||nodo.val=="&&"||nodo.val=="||"||nodo.val=="??"||nodo.val=="!!"
                ||nodo.val=="+"||nodo.val=="-"||nodo.val=="/"||nodo.val=="*"||nodo.val=="^"){
            //System.out.println("E1");
            if(nodo.hijos.size()==2){ 
              // System.out.println("E1.1");
                parame(nodo.hijos.get(0),clase,met);
                cade+=nodo.val;
                parame(nodo.hijos.get(1),clase,met);
            }
        }else if(nodo.val.contains("valor")){
            //System.out.println("E2");
            cade+=nodo.hijos.get(1).val;
        }else if(nodo.val.contains("variable")){
            //System.out.println("E3");
            String val=verificar(nodo.hijos.get(0).val,clase,met);
            if(!"nulo".equals(val)){
              //  System.out.println("E4");
                cade+=val;
            }else{
                //System.out.println("E5");
                cade+=nodo.hijos.get(0).val;
            }
        }else{
            //System.out.println("E6");
            for(int i=0;i<nodo.hijos.size();i++){
                parame(nodo.hijos.get(i),clase,met);
            }
        }
//        System.out.println(cade);

            return cade;
}

boolean flag2=true; String tems=""; boolean flag3=false;
  
public void parame1(Nodo accion,int clase, int met){
    cade=""; String h="",h2="";
        switch(accion.val){
            
                    case "declaracion":
                        if(flag2==true){
            for(int y=0;y<accion.hijos.get(0).hijos.size();y++){
                if(metf==1){
                    for(int z=0;z<clases.get(clase).metodos.get(met).parametros.size();z++){
                        if(clases.get(clase).metodos.get(met).parametros.get(z).nombre.contains(accion.hijos.get(0).hijos.get(y).val)){
                            JOptionPane.showMessageDialog(null, "id: "+ accion.hijos.get(0).hijos.get(y).val+" ya declarada");
                        }else{
                            Variable nueva = new Variable();
                            nueva.nombre=accion.hijos.get(0).hijos.get(y).val;
                            nueva.acceso=clases.get(clase).metodos.get(met).nombre;
                            nueva.tipo=accion.hijos.get(1).val;
                           
                            clases.get(clase).metodos.get(met).parametros.add(nueva);
                        }
                    }
                    
                }else if(metf==2){
                    for(int z=0;z<clases.get(clase).funciones.get(met).parametros.size();z++){
                        if(clases.get(clase).funciones.get(met).parametros.get(z).nombre.contains(accion.hijos.get(0).hijos.get(y).val)){
                            JOptionPane.showMessageDialog(null, "id: "+ accion.hijos.get(0).hijos.get(y).val+" ya declarada");
                        }else{
                            Variable nueva = new Variable();
                            nueva.nombre=accion.hijos.get(0).hijos.get(y).val;
                            nueva.acceso=clases.get(clase).funciones.get(met).nombre;
                            nueva.tipo=accion.hijos.get(1).val;
                           
                            clases.get(clase).funciones.get(met).parametros.add(nueva);
                        }
                    }
                }
            }
                 
                        }
                        break;
                    case "asignacion":
                            if(flag2==true){
                            //txtConsola.append("-->Accion: Asignacion local: \n");
                            String id=(accion.hijos.get(0).val).trim();
                            String titmp=(accion.hijos.get(1).hijos.get(0).val).trim();
                            String exi=verificar(id.trim(),clase,met);
                            h=parame(accion.hijos.get(1),clase,met);

                            String vala=obtenerCondicion(h);
                            System.out.println(h+"  id: "+id+"   tipo "+titmp+"   valor "+vala);
                            boolean ac=false;
                if(metf==1){
                    for(int z=0;z<clases.get(clase).metodos.get(met).parametros.size();z++){
                        if(clases.get(clase).metodos.get(met).parametros.get(z).nombre.contains(id)){
                            clases.get(clase).metodos.get(met).parametros.get(z).valor=vala;
                            ac=true;
                        }
                    }
                    
                }else if(metf==2){
                    for(int z=0;z<clases.get(clase).funciones.get(met).parametros.size();z++){
                        if(clases.get(clase).funciones.get(met).parametros.get(z).nombre.contains(id)){
                            clases.get(clase).funciones.get(met).parametros.get(z).valor=vala;
                            ac=true;
                        }
                    }
                }
            if(ac==false){
                                for(int rr=0;rr<clases.get(clase).variables.size();rr++){
                                    System.out.println(clases.get(clase).variables.get(rr).nombre+" "+clases.get(clase).variables.get(rr).tipo);
                                            if(clases.get(clase).variables.get(rr).nombre.contains(id)){
                                                //if(globales.get(rr).tipo.contains(titmp)){
                                                        clases.get(clase).variables.get(rr).valor=vala;
                                                        ac=true;
                
                                                //}else{
                                                  // JOptionPane.showMessageDialog(null, "Error Semantico global: tipo no compatible");

                                                //}
                                            }
                                   }
                                if(ac==false){
                                JOptionPane.showMessageDialog(null, "Error Semantico: variable no declarada" );
                                }
                            }
                        
                        }
                        break;
                    case "print":
                        if(flag2==true){
                        //txtConsola.append("-->Accion: Print\n");
                        h=parame(accion.hijos.get(0),clase,met);
                        //txtConsola.append("parametros: "+h+"\n\n");
                        //txtConsola.append("Imprimio: "+obtenerCondicion(h)+"\n\n");
                        }
                        break;
                    
                    case "return":
                        //txtConsola.append("-->Accion: Return\n");
                        h=parame(accion.hijos.get(0),clase,met);
                        //txtConsola.append("parametros: "+h+"\n");
                        //Retorno=obtenerCondicion(h);
                        //txtConsola.append("Retorno: "+Retorno+"\n\n");
                        
                        break;
                
                    case "break":
                        //txtConsola.append("-->Accion: Break\n");
                        //txtConsola.append("Sale del ciclo\n\n");
                        flag2=false;
                        break;
                
                    case "if":
 
                        h=parame(accion.hijos.get(0),clase,met);
                        //txtConsola.append("parametros: "+h+"\n");
                        String tm=obtenerCondicion(h);
                        //txtConsola.append("condicion: "+tm+"\n\n");
                        
                        if(tm.contains("true")){flag2=true; flag3=true;}else{flag2=false;}
                        
                            for(int t=0;t<accion.hijos.get(1).hijos.size();t++){
                                    if(flag2==true){
                                    parame1(accion.hijos.get(1).hijos.get(t),clase,met);
                                    }
                            }
                        
                        break;

                    case "if1":
                        flag3=false;
                         //txtConsola.setFont(fuente);
                       //txtConsola.append("-->Accion: If\n");
                         //txtConsola.setFont(normal);
                       
                        for(int t=0;t<accion.hijos.size();t++){
                            parame1(accion.hijos.get(t),clase,met);
                        }
                        break;
                    
                    case "if2":
                                    flag3=false;            //txtConsola.setFont(fuente);
                        //txtConsola.append("-->Accion: If-Else\n");
                        //txtConsola.setFont(normal);
                        for(int t=0;t<accion.hijos.size();t++){
                            parame1(accion.hijos.get(t),clase,met);
                        }
                        break;
 
                    case "if3":                        
                        //txtConsola.setFont(fuente);
                        flag3=false;
                        //txtConsola.append("-->Accion: If-Elseif\n");
                                                //txtConsola.setFont(normal);
                        for(int t=0;t<accion.hijos.size();t++){
                            parame1(accion.hijos.get(t),clase,met);
                        }
                        break;
                        
                    case "if4":
                                                //txtConsola.setFont(fuente);
                        flag3=false;
                        //txtConsola.append("-->Accion: If-Elseif-Else\n");
                        //txtConsola.setFont(normal);

                        for(int t=0;t<accion.hijos.size();t++){
                            parame1(accion.hijos.get(t),clase,met);
                        }
                        break;

                    case "elseif":
                        
                            h=parame(accion.hijos.get(0),clase,met);
                            //txtConsola.append("parametros: "+h+"\n");
                            String tm2=obtenerCondicion(h);
                            //txtConsola.append("condicion: "+tm2+"\n\n");
                            if(tm2.contains("true")){flag2=true;}else{flag2=false;}
                        
                            for(int t=0;t<accion.hijos.get(1).hijos.size();t++){
                                    if(flag2==true){
                                    parame1(accion.hijos.get(1).hijos.get(t),clase,met);
                                    }
                            }
                        
                        break;
                    case "elseifs":
                            for(int o=0;o<accion.hijos.size();o++){
                            parame1(accion.hijos.get(o),clase,met);
                          
                            }
                        break;

                    case "else":
                        System.out.println("entro flag "+flag2);
                        if(flag2==false){
                            flag2=true;
                        for(int t=0;t<accion.hijos.get(0).hijos.size();t++){
                                parame1(accion.hijos.get(0).hijos.get(t),clase,met);
                          
                        }
                        }
                        break;
                    
                    case "while":
                        //txtConsola.setFont(fuente);
                        flag3=false;
                        //txtConsola.append("-->Accion: While\n");
                        //txtConsola.setFont(normal);
                        Nodo tempo=accion.hijos.get(0);
                        h=parame(tempo,clase,met);
                        //txtConsola.append("parametros: "+h+"\n");

                        String tmw="";
                        tmw=obtenerCondicion(h);
                        //txtConsola.append("condicion: "+tmw+"\n\n");
                        
                        if(tmw.contains("true")){flag2=true;}else{flag2=false;}
                        
                        while(flag2==true){
                            for(int t=0;t<accion.hijos.get(1).hijos.size();t++){
                                    parame1(accion.hijos.get(1).hijos.get(t),clase,met);
                            }                         
                            h=parame(tempo,clase,met);
                        //txtConsola.append("parametros: "+h+"\n");
                        tmw=obtenerCondicion(h);
                            //txtConsola.append("condicion while: "+tmw+"\n");
                            if(tmw.contains("true")){flag2=true;}else{flag2=false;}
                        }
                        break;
                    
                    case "switch":
                        flag3=false;
                        //txtConsola.append("-->Accion: Switch\n");
                        h=parame(accion.hijos.get(0),clase,met);
                        //txtConsola.append("parametros: "+h+"\n");
                        //txtConsola.append("condicion: "+obtenerCondicion(h)+"\n\n");
                        tems=obtenerCondicion(h);
                            parame1(accion.hijos.get(1),clase,met);
                        break;

                   case "cases":
                        for(int t=0;t<accion.hijos.size();t++){
                            parame1(accion.hijos.get(t),clase,met);
                        }
                        break;

                   case "case":
                       
                           System.out.println("entro case");
                            h=parame(accion.hijos.get(0),clase,met);
                            String tm2b=obtenerCondicion(h);
                            //txtConsola.append("condicion: "+tm2b+"\n\n");
                            if(tems.contains(tm2b)){flag2=true;flag3=true;}else{flag2=false;}
                        
                            for(int t=0;t<accion.hijos.get(1).hijos.size();t++){
                                    if(flag2==true){
                                        parame1(accion.hijos.get(1).hijos.get(t),clase,met);
                                    }
                            }
                       
                            break;
                   case "def":
                        System.out.println("entro def "+flag2);
                        if(flag3==false){
                            flag2=true;
                        for(int t=0;t<accion.hijos.get(0).hijos.size();t++){
                                parame1(accion.hijos.get(0).hijos.get(t),clase,met);
                          
                        }
                        }
                        break;
                    
                    case "for":
                        flag3=false;
                        //txtConsola.append("-->Accion: For\n");
                        h=parame(accion.hijos.get(0),clase,met);
                        cade="";
                        h2=parame(accion.hijos.get(1),clase,met);
                        String desde=obtenerCondicion(h);
                        String hasta=obtenerCondicion(h2);
                         //txtConsola.append("Desde: "+desde+"   hasta: "+hasta+"\n\n");
                        double value1 = Double.parseDouble(desde);
                        double value2 = Double.parseDouble(hasta);
                         for(double i=value1;i<=value2;i++){
                      
                             for(int t=0;t<accion.hijos.get(2).hijos.size();t++){
                                parame1(accion.hijos.get(2).hijos.get(t),clase,met);
                          
                            }
                             
                        }
                        
                        break;

                    case "dowhile":
                                                //txtConsola.setFont(fuente);

                        //txtConsola.append("-->Accion: Do-While\n");
                        flag3=false;
                        //txtConsola.setFont(normal);
                        Nodo tempo2=accion.hijos.get(1);
                        h=parame(tempo2,clase,met);
                        //txtConsola.append("parametros: "+h+"\n");

                        String tmw2="";
                        tmw2=obtenerCondicion(h);
                        //txtConsola.append("condicion do-while: "+tmw2+"\n\n");
                            for(int t=0;t<accion.hijos.get(0).hijos.size();t++){
                                    parame1(accion.hijos.get(0).hijos.get(t),clase,met);
                            }
                        if(tmw2.contains("true")){flag2=true;}else{flag2=false;}
                        
                        while(flag2==true){
                            for(int t=0;t<accion.hijos.get(0).hijos.size();t++){
                                    parame1(accion.hijos.get(0).hijos.get(t),clase,met);
                            }                         
                            h=parame(tempo2,clase,met);
                        //txtConsola.append("parametros: "+h+"\n");
                        tmw2=obtenerCondicion(h);
                            //txtConsola.append("condicion do-while: "+tmw2+"\n");
                            if(tmw2.contains("true")){flag2=true;}else{flag2=false;}
                        }

                        break;
                        
                    case "repeat":
       
                        //txtConsola.setFont(fuente);
                        flag3=false;
                        //txtConsola.append("-->Accion: Repeat\n");
                        //txtConsola.setFont(normal);
                        Nodo tempo3=accion.hijos.get(1);
                        h=parame(tempo3,clase,met);
                        //txtConsola.append("parametros: "+h+"\n");

                        String tmw3="";
                        tmw3=obtenerCondicion(h);
                        //txtConsola.append("condicion: "+tmw3+"\n\n");
                            for(int t=0;t<accion.hijos.get(0).hijos.size();t++){
                                    parame1(accion.hijos.get(0).hijos.get(t),clase,met);
                            }
                        if(tmw3.contains("true")){flag2=true;}else{flag2=false;}
                        
                        while(flag2==false){
                            for(int t=0;t<accion.hijos.get(0).hijos.size();t++){
                                    parame1(accion.hijos.get(0).hijos.get(t),clase,met);
                            }                         
                            h=parame(tempo3,clase,met);
                        //txtConsola.append("parametros: "+h+"\n");
                        tmw3=obtenerCondicion(h);
                            //txtConsola.append("condicion repeat: "+tmw3+"\n");
                            if(tmw3.contains("true")){flag2=true;}else{flag2=false;}
                        }
                        break;
                        
                    case "loop":
                        //txtConsola.setFont(fuente);
                        //txtConsola.setForeground(Color.BLUE);
                        flag3=false;
                        //txtConsola.append("-->Accion: Loop\n");
                        //txtConsola.setFont(normal);
                        //txtConsola.setForeground(Color.BLACK);
                        Nodo tempo4=accion.hijos.get(1);
                        h=parame(tempo4,clase,met);
                        //txtConsola.append("parametros: "+h+"\n");

                        String tmw4="";
                        tmw4=obtenerCondicion(h);
                        //txtConsola.append("condicion: "+tmw4+"\n\n");
                        
                        if(tmw4.contains("true")){flag2=true;}else{flag2=false;}
                        
                        while(flag2==true){
                        tmw4=obtenerCondicion(h);
                        if(tmw4.contains("true")){flag2=true;}else{flag2=false;}
                            for(int t=0;t<accion.hijos.get(0).hijos.size();t++){
                                    parame1(accion.hijos.get(0).hijos.get(t),clase,met);
                            }                         
                            h=parame(tempo4,clase,met);
                        //txtConsola.append("parametros: "+h+"\n");
                        //txtConsola.append("condicion loop: "+tmw4+"\n");
                            
                        }
                        
                        break;

                    default:
                        break;
                }
        cade="";

    }



public String verificar(String value,int clase,int met){
        String h="";
        boolean ht=false;
        //si esta en las locales del metodo
        for(int i=0;i<clases.get(clase).metodos.get(met).parametros.size();i++){
            Variable local = clases.get(clase).metodos.get(met).parametros.get(i);
            if(local.nombre.contains(value)){
                if(!"".equals(local.valor)){
                    h=local.valor;
                }
                ht=true;
                
            }
        }
        if(ht==false){
            //si esta en las globales de la clase
            for(int i=0;i<clases.get(clase).variables.size();i++){
                if(clases.get(clase).variables.get(i).nombre.contains(value)){
                    if(!"".equals(clases.get(clase).variables.get(i).valor)){
                            h=clases.get(clase).variables.get(i).valor;
                    }else{
                    JOptionPane.showMessageDialog(null, "id: "+clases.get(clase).variables.get(i).nombre+" no asignada, sin valor" );
                    }
                ht=true;
                }
            }
        }
        if(ht==false){
            JOptionPane.showMessageDialog(null, "id: "+value+" no declarada" );
        }
        if("".equals(h)){
            h="nulo";
          // JOptionPane.showMessageDialog(null, "No existe nombre de id, se tomara como string" );

        }
        return h;
    }

public void ordenarMetodos(){
    System.out.println("------Ordenando Metodos/Funciones---------------");
    Metodos_Funciones tmp;
   
    for(int i=0; i<metodostemporales.size();i++){
        
        tmp=metodostemporales.get(i);
 
        if(tmp.tipo.equals("metodo")){
            for(int j=0;j<clases.size();j++){
                
                if(clases.get(j).nombre.equals(tmp.nivel)){
                   clases.get(j).metodos.add(tmp);
                }
            }
                
        }else if(tmp.tipo.equals("funcion")){
                
            for(int j=0;j<clases.size();j++){
                if(clases.get(j).nombre.equals(tmp.nivel)){
                    clases.get(j).funciones.add(tmp);
                }
            }  
        }else if(tmp.tipo.equals("constructor")){
             
            for(int j=0;j<clases.size();j++){
                if(clases.get(j).nombre.equals(tmp.nivel)){
                    
                    clases.get(j).constructor=tmp;
                }
            }     
        }
    }
    metodostemporales.clear();
}

public void extenderMetodos(){
    for(int i=0;i<clases.size();i++){
        Clase subclase;
        subclase= clases.get(i);
        for(int j=0;j<clases.size();j++){
            Clase superclase;
            superclase= clases.get(j);
            //si extiende
            if(subclase.extiende.equals(superclase.nombre)){
                for(int k=0; k<superclase.metodos.size();k++){
                    //si se tiene acceso
                    if(superclase.metodos.get(k).acceso.equals("publico")||superclase.metodos.get(k).acceso.equals("protegido")){
                        //reglas override
                        boolean over=false;
                        Metodos_Funciones evaluar3 = new Metodos_Funciones();
                        for(int l=0; l<subclase.metodos.size();l++){
                           Metodos_Funciones evaluar= subclase.metodos.get(l);
                           Metodos_Funciones evaluar2=superclase.metodos.get(k); 
                        //si subclase contiene metodo nombre igual al superclase
                           if(evaluar.nombre.equals(evaluar2.nombre)){
                               //si numero de parametros es la misma
                               if(evaluar.parametros.size()==evaluar2.parametros.size()){
                                   //si tipo de retorno es el mismo
                                   if(evaluar.tipo_ret.equals(evaluar2.tipo_ret)){
                                       System.out.println("---> OVERRIDE EXITOSO");
                                        over=true;
                                   }
                               }
                           evaluar3=evaluar; 
                           }
                        }
                        
                    if(over==false){
                        if(!evaluar3.nombre.equals("")){
                            subclase.metodos.remove(evaluar3);
                        }
                        subclase.metodos.add(superclase.metodos.get(k));
                    
                    }
                    over=false;
                    }
                }
                    
                
                for(int k=0; k<superclase.funciones.size();k++){
                    boolean over=false;
                    //si se tiene acceso
                    Metodos_Funciones evaluar3= new Metodos_Funciones();
                    if(superclase.funciones.get(k).acceso.equals("publico")||superclase.funciones.get(k).acceso.equals("protegido")){
                    for(int l=0; l<subclase.funciones.size();l++){
                    Metodos_Funciones evaluar= subclase.funciones.get(l);
                    Metodos_Funciones evaluar2=superclase.funciones.get(k); 
                        //si subclase contiene metodo nombre igual al superclase
                           if(evaluar.nombre.equals(evaluar2.nombre)){
                               //si numero de parametros es la misma
                               if(evaluar.parametros.size()==evaluar2.parametros.size()){
                                   //si tipo de retorno es el mismo
                                   if(evaluar.tipo_ret.equals(evaluar2.tipo_ret)){

                                       System.out.println("---> OVERRIDE EXITOSO");
                                        over=true;
                                   }
                               }
                           evaluar3=evaluar; 
                           }
                        }
                    if(over==false){
                        if(!evaluar3.nombre.equals("")){
                            subclase.funciones.remove(evaluar3);
                        }
                        subclase.funciones.add(superclase.funciones.get(k));
                    
                    }
                    over=false;
                    }
                }
                 
                
                subclase.constructor_extiende=superclase.constructor;
            }
        }    
    }
}

public void pruebas(){
    System.out.println("--------------prueba de clases --------\n");
    System.out.println("Numero de Clases: "+clases.size());
    
    for(int i=0; i<clases.size();i++){
        System.out.println("\nNombre: "+clases.get(i).nombre);
        System.out.println("Extiende: "+clases.get(i).extiende);
        System.out.println("Constructor: "+clases.get(i).constructor.nombre);
        System.out.println("Constructor_Extiende: "+clases.get(i).constructor_extiende.nombre);
        System.out.println("Metodos: "+clases.get(i).metodos.size());
            for(int j=0; j<clases.get(i).metodos.size();j++){
                System.out.println("\t"+clases.get(i).metodos.get(j).nombre+"  nivel: "+clases.get(i).metodos.get(j).nivel);
            }
        System.out.println("Funciones: "+clases.get(i).funciones.size());
            for(int j=0; j<clases.get(i).funciones.size();j++){
                System.out.println("\t"+clases.get(i).funciones.get(j).nombre+"  nivel: "+clases.get(i).funciones.get(j).nivel);
            }
       System.out.println("Variables: "+clases.get(i).variables.size());            
       for(int j=0; j<clases.get(i).variables.size();j++){
                System.out.println("\t"+clases.get(i).variables.get(j).nombre+"\tval: "+clases.get(i).variables.get(j).valor);
       }
    }

}


}


