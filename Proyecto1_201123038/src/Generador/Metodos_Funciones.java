/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Generador;

import java.util.ArrayList;

/**
 *
 * @author Yess
 */
public class Metodos_Funciones {
    
    String nombre;
    String acceso;
    String tipo;
    String tipo_ret;
    ArrayList<Instruccion> instrucciones;
    ArrayList<Variable> parametros;
    Nodo sentencias;
    String retorno;
    String nivel;
    boolean override;
    
    Metodos_Funciones(){
        nombre="";
        acceso="publico";
        tipo="";
        instrucciones=new ArrayList<>();
        parametros = new ArrayList<>();
        sentencias=new Nodo();
        retorno="";
        tipo_ret="";
        nivel="";
        override=false;
    }
}
