/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Generador;

import java.util.ArrayList;
/**
 *
 * @author Yess
 */
public class Clase {

    ArrayList<Metodos_Funciones> metodos;
    ArrayList<Metodos_Funciones> funciones;
    Metodos_Funciones principal;
    Metodos_Funciones constructor;
    Metodos_Funciones constructor_extiende;
    String extiende;
    String nombre;
    ArrayList<Variable> variables;
    
    Clase(){
        metodos= new ArrayList<>();
        funciones= new ArrayList<>();
        principal= new Metodos_Funciones();
        constructor= new Metodos_Funciones();
        constructor_extiende= new Metodos_Funciones();
        extiende="";
        nombre="";
        variables= new ArrayList<>();
    }
}
