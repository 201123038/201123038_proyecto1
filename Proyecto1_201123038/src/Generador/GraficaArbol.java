/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Generador;

import java.awt.Desktop;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author Yess
 */
public class GraficaArbol {
    int contador=0; 
    String relacion="";
    public String graphviz="";
    
 public void graficarAst(Nodo raiz){

        relacion="";    
        contador=0; 
        graphviz="";
        graphviz+="\ndigraph G {\r\nnode [shape=rectangle, fontcolor=black];\n";
        lNodos(raiz);
        eNodos(raiz);
        graphviz+="}";
        
        
        FileWriter archivo = null;
        @SuppressWarnings("UnusedAssignment")
        PrintWriter printwriter =null;
        try {    
            archivo = new FileWriter("C:\\compi_vacas\\ast.dot");
            printwriter = new PrintWriter(archivo);    
            printwriter.println(graphviz);         
            printwriter.close();
            } catch (IOException ex) {
            }
        
        try{
            String dotPath="C:\\Program Files (x86)\\Graphviz2.38\\bin\\dot.exe";
            String fileInputPat ="C:\\compi_vacas\\ast.dot";
            String fileOutputPath="C:\\compi_vacas\\ast.jpg";

            String tParam = "-Tjpg";
            String tOParam = "-o";
        
            String[] cmd = new String[5];
            cmd[0] = dotPath;
            cmd[1] = tParam;
            cmd[2] = fileInputPat;
            cmd[3] = tOParam;
            cmd[4] = fileOutputPath;
                  
            Runtime rt = Runtime.getRuntime();
      
            rt.exec( cmd );
      
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
            }
     Desktop desktop;
         File file = new File("C:\\compi_vacas\\ast.jpg");//declaro un Objeto File que apunte a mi archivo html
        if (Desktop.isDesktopSupported()){// si éste Host soporta esta API 
           desktop = Desktop.getDesktop();//objtengo una instancia del Desktop(Escritorio)de mi host 
            try {            
                desktop.open(file);//abro el archivo con el programa predeterminado
            } catch (IOException ex) {
                Logger.getLogger(Principal.class.getName()).log(Level.SEVERE, null, ex);
            }
                
        }
       else{ JOptionPane.showMessageDialog(null,"Lo lamento,no se puede abrir el archivo; ");
       }
    }
    
    public void lNodos(Nodo praiz)
    {        
        graphviz += "node" + contador + "[label=\"" + praiz.val + "\"];\n";
        praiz.num = contador;  
        contador++;        
        for(Nodo temp:praiz.hijos){
            lNodos(temp);
        }
    } 
    
    public void eNodos(Nodo praiz)
    {        
        for(Nodo temp:praiz.hijos){
            relacion = "\"node" + praiz.num + "\"->";
            relacion += "\"node" + temp.num + "\";";
            graphviz += relacion+"\n";
            eNodos(temp);
        }
    }    
}
