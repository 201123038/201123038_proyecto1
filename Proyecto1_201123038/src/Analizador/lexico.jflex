package Analizador;
import java_cup.runtime.Symbol;
import javax.swing.JOptionPane;
%%

%state COMENT
%state MULTI

/* ---==: Opciones y declaraciones :==---*/
%cupsym Simbolos 
%class Lexico 
%cup 
%public
%line
%column
%char
%ignorecase
%class scanner
%unicode
%full


/*---==: Expresiones :==---*/

Entero = [0-9]+ 
Numero = ("-")?{Entero}("." {Entero})*
letra =[a-zA-Z??]+
id = {letra}({letra}|{Entero}|"_")*
idchar = "'" ~ "'"
idstring = "\"" ~ "\""


%%
/*---==: Reglas lexicas :==---*/

//--- General ---

<YYINITIAL> "%%"  { return new Symbol(Simbolos.sec, yychar,yyline,new String(yytext())); }
<YYINITIAL> ","   { return new Symbol(Simbolos.coma, yychar,yyline,new String(yytext()));}
<YYINITIAL> ";"   { return new Symbol(Simbolos.pcoma, yychar,yyline,new String(yytext()));}
<YYINITIAL> "."   { return new Symbol(Simbolos.punto, yychar,yyline,new String(yytext()));}


//--- Declaraciones ---

<YYINITIAL> "terminal"      { return new Symbol(Simbolos.termi, yychar,yyline,new String(yytext())); }
<YYINITIAL> "no"            { return new Symbol(Simbolos.no, yychar,yyline,new String(yytext())); }
<YYINITIAL> "precedencia"   { return new Symbol(Simbolos.prece, yychar,yyline,new String(yytext())); }
<YYINITIAL> "asociatividad" { return new Symbol(Simbolos.asocia, yychar,yyline,new String(yytext())); }
<YYINITIAL> "iniciar"       { return new Symbol(Simbolos.inicia, yychar,yyline,new String(yytext())); }
<YYINITIAL> "con"           { return new Symbol(Simbolos.con, yychar,yyline,new String(yytext())); }
<YYINITIAL> "izq"           { return new Symbol(Simbolos.izq, yychar,yyline,new String(yytext())); }
<YYINITIAL> "der"           { return new Symbol(Simbolos.der, yychar,yyline,new String(yytext())); }
<YYINITIAL> "int"           { return new Symbol(Simbolos.intt, yychar,yyline,new String(yytext())); }
<YYINITIAL> "char"          { return new Symbol(Simbolos.charr, yychar,yyline,new String(yytext())); }
<YYINITIAL> "double"        { return new Symbol(Simbolos.doublee, yychar,yyline,new String(yytext())); }
<YYINITIAL> "string"        { return new Symbol(Simbolos.string, yychar,yyline,new String(yytext())); }
<YYINITIAL> "bool"          { return new Symbol(Simbolos.bool, yychar,yyline,new String(yytext())); }
<YYINITIAL> "float"          { return new Symbol(Simbolos.flot, yychar,yyline,new String(yytext())); }
<YYINITIAL> "public"        { return new Symbol(Simbolos.rpublic, yychar,yyline,new String(yytext())); }
<YYINITIAL> "private"          { return new Symbol(Simbolos.rprivate, yychar,yyline,new String(yytext())); }
<YYINITIAL> "protected"          { return new Symbol(Simbolos.rprotected, yychar,yyline,new String(yytext())); }

 
//--- Gramatica ---

<YYINITIAL> "::="   { return new Symbol(Simbolos.dpi, yychar,yyline,new String(yytext()));}
<YYINITIAL> "|"   { return new Symbol(Simbolos.pipe, yychar,yyline,new String(yytext()));}
<YYINITIAL> "{:"   { return new Symbol(Simbolos.acabi, yychar,yyline,new String(yytext()));}
<YYINITIAL> ":}"   { return new Symbol(Simbolos.accer, yychar,yyline,new String(yytext()));}
<YYINITIAL> "$"   { return new Symbol(Simbolos.dolar, yychar,yyline,new String(yytext()));}
<YYINITIAL> "$$"   { return new Symbol(Simbolos.ddolar, yychar,yyline,new String(yytext()));}
<YYINITIAL> "<!Override!>"   { return new Symbol(Simbolos.over, yychar,yyline,new String(yytext()));}


//--- Codigo ---

<YYINITIAL> ":"   { return new Symbol(Simbolos.dpun, yychar,yyline,new String(yytext()));}
<YYINITIAL> "["   { return new Symbol(Simbolos.corabi, yychar,yyline,new String(yytext()));}
<YYINITIAL> "]"   { return new Symbol(Simbolos.corcer, yychar,yyline,new String(yytext()));}
<YYINITIAL> "++"  { return new Symbol(Simbolos.dmas, yychar,yyline,new String(yytext()));}
<YYINITIAL> "--"  { return new Symbol(Simbolos.dmenos, yychar,yyline,new String(yytext()));}
<YYINITIAL> "+"   { return new Symbol(Simbolos.mas, yychar,yyline,new String(yytext()));}
<YYINITIAL> "-"   { return new Symbol(Simbolos.menos, yychar,yyline,new String(yytext()));}
<YYINITIAL> "*"   { return new Symbol(Simbolos.por, yychar,yyline,new String(yytext()));}
<YYINITIAL> "/"   { return new Symbol(Simbolos.div, yychar,yyline,new String(yytext()));}
<YYINITIAL> "^"   { return new Symbol(Simbolos.pot, yychar,yyline,new String(yytext()));}
<YYINITIAL> "{"   { return new Symbol(Simbolos.llavabi, yychar,yyline,new String(yytext()));}
<YYINITIAL> "}"   { return new Symbol(Simbolos.llavcer, yychar,yyline,new String(yytext()));}
<YYINITIAL> "("   { return new Symbol(Simbolos.parabi, yychar,yyline,new String(yytext()));}
<YYINITIAL> ")"   { return new Symbol(Simbolos.parcer, yychar,yyline,new String(yytext()));}
<YYINITIAL> ">"   { return new Symbol(Simbolos.mayq, yychar,yyline,new String(yytext()));}
<YYINITIAL> "<"   { return new Symbol(Simbolos.menq, yychar,yyline,new String(yytext()));}
<YYINITIAL> ">="  { return new Symbol(Simbolos.mayigu, yychar,yyline,new String(yytext()));}
<YYINITIAL> "<="  { return new Symbol(Simbolos.menigu, yychar,yyline,new String(yytext()));}
<YYINITIAL> "=="  { return new Symbol(Simbolos.iguigu, yychar,yyline,new String(yytext()));}
<YYINITIAL> "!="  { return new Symbol(Simbolos.noigu, yychar,yyline,new String(yytext()));}
<YYINITIAL> "||"  { return new Symbol(Simbolos.or, yychar,yyline,new String(yytext()));}
<YYINITIAL> "??"  { return new Symbol(Simbolos.xor, yychar,yyline,new String(yytext()));}
<YYINITIAL> "&&"  { return new Symbol(Simbolos.and, yychar,yyline,new String(yytext()));}
<YYINITIAL> "!!"  { return new Symbol(Simbolos.not, yychar,yyline,new String(yytext()));}
<YYINITIAL> "true"      { return new Symbol(Simbolos.verdadero, yychar,yyline,new String(yytext())); }
<YYINITIAL> "false"     { return new Symbol(Simbolos.falso, yychar,yyline,new String(yytext())); }
<YYINITIAL> {idchar}    { return new Symbol(Simbolos.idchar, yychar,yyline,new String(yytext())); }
<YYINITIAL> {idstring}  { return new Symbol(Simbolos.idstring, yychar,yyline,new String(yytext())); }
<YYINITIAL> "="         { return new Symbol(Simbolos.igu, yychar,yyline,new String(yytext()));}
<YYINITIAL> "void"     { return new Symbol(Simbolos.metod, yychar,yyline,new String(yytext()));}
<YYINITIAL> "class"     { return new Symbol(Simbolos.clase, yychar,yyline,new String(yytext()));}
<YYINITIAL> "extends"     { return new Symbol(Simbolos.extiende, yychar,yyline,new String(yytext()));}
<YYINITIAL> "begin"     { return new Symbol(Simbolos.begin, yychar,yyline,new String(yytext()));}
<YYINITIAL> "end"       { return new Symbol(Simbolos.end, yychar,yyline,new String(yytext()));}
<YYINITIAL> "return"    { return new Symbol(Simbolos.returnn, yychar,yyline,new String(yytext()));}
<YYINITIAL> "print"     { return new Symbol(Simbolos.print, yychar,yyline,new String(yytext()));}
<YYINITIAL> "break"     { return new Symbol(Simbolos.breakk, yychar,yyline,new String(yytext()));}
<YYINITIAL> "continue"     { return new Symbol(Simbolos.conti, yychar,yyline,new String(yytext()));}
<YYINITIAL> "if"        { return new Symbol(Simbolos.iff, yychar,yyline,new String(yytext()));}
<YYINITIAL> "then"      { return new Symbol(Simbolos.then, yychar,yyline,new String(yytext()));}
<YYINITIAL> "elseif"    { return new Symbol(Simbolos.elseif, yychar,yyline,new String(yytext()));}
<YYINITIAL> "else"      { return new Symbol(Simbolos.elsee, yychar,yyline,new String(yytext()));}
<YYINITIAL> "endif"     { return new Symbol(Simbolos.endif, yychar,yyline,new String(yytext()));}
<YYINITIAL> "switch"    { return new Symbol(Simbolos.switchh, yychar,yyline,new String(yytext()));}
<YYINITIAL> "case"      { return new Symbol(Simbolos.casee, yychar,yyline,new String(yytext()));}
<YYINITIAL> "default"       { return new Symbol(Simbolos.def, yychar,yyline,new String(yytext()));}
<YYINITIAL> "endswitch" { return new Symbol(Simbolos.endswitch, yychar,yyline,new String(yytext()));}
<YYINITIAL> "while"     { return new Symbol(Simbolos.whilee, yychar,yyline,new String(yytext()));}
<YYINITIAL> "do"        { return new Symbol(Simbolos.doo, yychar,yyline,new String(yytext()));}
<YYINITIAL> "endwhile"  { return new Symbol(Simbolos.endwhile, yychar,yyline,new String(yytext()));}
<YYINITIAL> "repeat"  { return new Symbol(Simbolos.repeat, yychar,yyline,new String(yytext()));}
<YYINITIAL> "until"  { return new Symbol(Simbolos.enduntil, yychar,yyline,new String(yytext()));}
<YYINITIAL> "for"  { return new Symbol(Simbolos.forr, yychar,yyline,new String(yytext()));}
<YYINITIAL> "endfor"  { return new Symbol(Simbolos.endfor, yychar,yyline,new String(yytext()));}
<YYINITIAL> "loop"  { return new Symbol(Simbolos.loop, yychar,yyline,new String(yytext()));}
<YYINITIAL> "endloop"  { return new Symbol(Simbolos.endloop, yychar,yyline,new String(yytext()));}


//--- Comentarios ---

<YYINITIAL> "//"   {yybegin(COMENT);}
<COMENT> .|[ \t\f]  {}
<COMENT> "\n"       {yybegin(YYINITIAL);}

<YYINITIAL> "/*"     { yybegin(MULTI);}
<MULTI> .|[ \t\f\r\n] {}
<MULTI>   "*/"       {yybegin(YYINITIAL);}



//--- Universales ---
<YYINITIAL> {Numero}  { return new Symbol(Simbolos.num, yychar,yyline,new String(yytext())); }
<YYINITIAL> {id}      { return new Symbol(Simbolos.id, yychar,yyline,new String(yytext())); }

//--- Ignorar Espacios ---
<YYINITIAL> [ \t\r\f\n]+        { /* Se ignoran */}


/* Cualquier Otro */
<YYINITIAL> .  { JOptionPane.showMessageDialog(null, "Error lexico:  " + new String(yytext()) + " Columna: " + yychar + " Linea: " + yyline);
                /*return new Symbol(Simbolos.erlex, yychar,yyline,new String(yytext()));*/} 

